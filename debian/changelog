rickslab-gpu-utils (3.9.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 3.9.0

 -- Andrius Merkys <merkys@debian.org>  Mon, 10 Jun 2024 08:06:07 -0400

rickslab-gpu-utils (3.8.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 3.8.0
  * Bump copyright years.

 -- Andrius Merkys <merkys@debian.org>  Fri, 21 Oct 2022 02:27:27 -0400

rickslab-gpu-utils (3.6.0-3) unstable; urgency=medium

  * Team upload.
  * Install icons.
  * Bump copyright years.
  * Move transitional package ricks-amdgpu-utils to oldlibs/optional per
    policy 4.0.1.
  * Update standards version to 4.6.0, no changes needed.

 -- Andrius Merkys <merkys@debian.org>  Fri, 29 Apr 2022 08:25:34 -0400

rickslab-gpu-utils (3.6.0-2) unstable; urgency=medium

  * Team upload.
  * Bumping debhelper-compat (no changes).

 -- Andrius Merkys <merkys@debian.org>  Thu, 11 Nov 2021 04:14:03 -0500

rickslab-gpu-utils (3.6.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 3.6.0
  * Renaming the source and binary packages.

 -- Andrius Merkys <merkys@debian.org>  Mon, 06 Sep 2021 08:17:20 -0400

ricks-amdgpu-utils (3.5.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 3.5.0
  * Fixing split of the installed files to binary packages.
  * Wrapping and sorting.
  * Defining Breaks+Replaces relationship between current ricks-amdgpu-utils
    and older python3-gpumodules (moved manpages).
  * Dropping reference to nonexistent ricks-amdgpu-utils-doc package.

 -- Andrius Merkys <merkys@debian.org>  Wed, 12 Aug 2020 02:31:01 -0400

ricks-amdgpu-utils (3.0.0-1) unstable; urgency=medium

  * Team upload.

  [ Steffen Moeller ]
  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 12 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andrius Merkys <merkys@debian.org>  Thu, 09 Apr 2020 00:13:25 -0400

ricks-amdgpu-utils (2.5.2-1) unstable; urgency=medium

  * Initial release (Closes: #932554)

 -- Steffen Möller <moeller@debian.org>  Fri, 19 Jul 2019 01:00:13 +0200
